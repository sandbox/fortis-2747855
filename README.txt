CONTENTS OF THIS FILE
=====================

 * Introduction
 * Information
 * Installation
 * Maintainers

INTRODUCTION
============

The Adyax_Test module implements simple REST Service.

INFORMATION
============

Task:
Without using any contributed modules, create custom module that implements
simple REST Service.
Web service URL should be: /adyax_ws
It should support next methods:
 * GET - returns JSON with data from node. NID provided in URL request, for
example: /adyax_ws?nid=123;
 * POST - retrieve JSON data with title, type and body and create new node based
on provided data;
 * PUT - retrieve JSON data and update node by NID provided in URL or JSON
(up to you);
 * DELETE - deletes node by provided NID

INSTALLATION
============

Download with:

git clone fortis@git.drupal.org:sandbox/fortis/2747855.git adyax_test
cd adyax_test

Then place and enable just like any other module.

MAINTAINERS
============

 * Alan Bondarchuk (fortis) - https://www.drupal.org/u/fortis

<?php

/**
 * @file
 * RESTful Resource Controller.
 */

/**
 * Specifies CRUD methods for resource.
 */
class AdyaxTestResourceController {

  /**
   * Gets the representation of a resource.
   */
  public function viewResource($id) {
    if (!$node = node_load($id)) {
      throw new AdyaxTestException('Resource not found', 404);
    }

    $function = __FUNCTION__;
    drupal_alter('adyax_test_response', $node, $function);
    return drupal_json_encode($node);
  }

  /**
   * Creates a new resource.
   */
  public function createResource($values) {
    // Make sure that type is present.
    if (!array_key_exists('type', $values)) {
      throw new AdyaxTestException('Missing type property', 406);
    }

    // Prevent duplicate inserts.
    if (array_key_exists('nid', $values)) {
      throw new AdyaxTestException('Unable to create resource with nid property', 406);
    }

    try {
      $info = entity_get_info('node');
      $node_keys = array_filter($info['entity keys']);
      $creation_values = array_intersect_key($values, array_flip($node_keys));
      $node = (object) $creation_values;
      // Set entity values.
      foreach ($values as $name => $value) {
        if (!in_array($name, $info['entity keys'])) {
          $node->{$name} = $value;
        }
      }
    }
    catch (Exception $e) {
      throw new AdyaxTestException($e->getMessage(), 406);
    }
    node_save($node);

    $function = __FUNCTION__;
    drupal_alter('adyax_test_response', $node, $function);
    return drupal_json_encode($node);
  }

  /**
   * Updates a resource.
   */
  public function updateResource($values) {
    if (!$node = node_load($values['nid'])) {
      throw new AdyaxTestException('Resource not found', 404);
    }

    try {
      foreach ($values as $name => $value) {
        if (in_array($name, array('nid', 'type'))) {
          // We don't allow changing the entity ID or bundle.
          if ($node->{$name} != $value) {
            throw new AdyaxTestException('Unable to change ' . $name, 422);
          }
        }
        else {
          $node->{$name} = $value;
        }
      }
    }
    catch (Exception $e) {
      throw new AdyaxTestException($e->getMessage(), 406);
    }
    node_save($node);

    // Return an updated representation by default.
    $function = __FUNCTION__;
    drupal_alter('adyax_test_response', $node, $function);
    return drupal_json_encode($node);
  }

  /**
   * Deletes a resource.
   */
  public function deleteResource($values) {
    node_delete($values['nid']);

    $value = array();
    $function = __FUNCTION__;
    drupal_alter('adyax_test_response', $value, $function, $values['nid']);
    return drupal_json_encode($value);
  }

}
